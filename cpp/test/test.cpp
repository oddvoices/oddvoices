#include <filesystem>
#include <vector>

#define CATCH_CONFIG_RUNNER
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_session.hpp>

#include "liboddvoices.hpp"
#include "g2p.hpp"

constexpr char k_leftBrace = '{';
constexpr char k_rightBrace = '}';

std::string g_cmudict = "cmudict-0.7b";


TEST_CASE("parsePronunciation works on single-letter phonemes") {
    auto text = "@DEINSTUZbdfghijklmnprstvwz_";
    std::vector<std::string> expected = {
        "@",
        "D",
        "E",
        "I",
        "N",
        "S",
        "T",
        "U",
        "Z",
        "b",
        "d",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "p",
        "r",
        "s",
        "t",
        "v",
        "w",
        "z",
        "_"
    };
    auto actual = oddvoices::g2p::parsePronunciation(text);
    REQUIRE(expected == actual);
}

TEST_CASE("parsePronunciation works on two-letter phonemes") {
    auto text = "@`OIaIaUdZeIoU{}";
    std::vector<std::string> expected = {
        "@`",
        "OI",
        "aI",
        "aU",
        "dZ",
        "eI",
        "oU",
        "{}"
    };
    auto actual = oddvoices::g2p::parsePronunciation(text);
    REQUIRE(expected == actual);
}

TEST_CASE("parsePronunciation works on phoneme aliases") {
    std::string text = "?V3`O&";
    text.push_back(k_leftBrace);
    std::vector<std::string> expected = { "_", "@", "@`", "A", "{}", "{}" };
    auto actual = oddvoices::g2p::parsePronunciation(text);
    REQUIRE(expected == actual);
}

TEST_CASE("parsePronunciation processes /Or/ and /Ar/ differently") {
    auto text = "OrAr";
    std::vector<std::string> expected = { "oU", "r", "A", "r" };
    auto actual = oddvoices::g2p::parsePronunciation(text);
    REQUIRE(expected == actual);
}

TEST_CASE("parsePronunciation ignores unrecognized characters") {
    auto text = "eqI";
    std::vector<std::string> expected = { "I" };
    auto actual = oddvoices::g2p::parsePronunciation(text);
    REQUIRE(expected == actual);
}

TEST_CASE("pronounceWord works on X-SAMPA notation") {
    oddvoices::g2p::G2P g2p(g_cmudict);
    auto text = "/l@v/";
    std::vector<std::string> expected = { "_", "l", "@", "v", "_" };
    auto actual = g2p.pronounceWord(text);
    REQUIRE(expected == actual);
}

TEST_CASE("pronounceWord works on dictionary words") {
    oddvoices::g2p::G2P g2p(g_cmudict);
    auto text = "gamer";
    std::vector<std::string> expected = { "_", "g", "eI", "m", "@`", "_" };
    auto actual = g2p.pronounceWord(text);
    REQUIRE(expected == actual);
}

TEST_CASE("pronounceWord works on exception words") {
    oddvoices::g2p::G2P g2p(g_cmudict);
    auto text = "and";
    std::vector<std::string> expected = { "_", "{}", "n", "d", "_" };
    auto actual = g2p.pronounceWord(text);
    REQUIRE(expected == actual);
}

TEST_CASE("pronounceWord works on unknown words") {
    oddvoices::g2p::G2P g2p(g_cmudict);
    auto text = "flargunnstow";
    std::vector<std::string> expected = { "_", "f", "l", "A", "r", "g", "@", "n", "s", "t", "aU", "_" };
    auto actual = g2p.pronounceWord(text);
    REQUIRE(expected == actual);
}

TEST_CASE("pronounceWord uses fixed VV diphones") {
    oddvoices::g2p::G2P g2p(g_cmudict);
    auto text = "zoe";
    std::vector<std::string> expected = { "_", "z", "oU", "w", "i", "_", };
    auto actual = g2p.pronounceWord(text);
    REQUIRE(expected == actual);
}

TEST_CASE("fixVVDiphones works") {
    std::vector<std::string> input = { "z", "oU", "i" };
    std::vector<std::string> expected = { "z", "oU", "w", "i" };
    auto actual = oddvoices::g2p::fixVVDiphones(input);
    REQUIRE(expected == actual);
}

TEST_CASE("Deque works") {
    int noValue = -1;
    std::vector<int> memory(10);
    oddvoices::Deque<int> deque(memory.data(), memory.size(), 0, 0, noValue);
    REQUIRE(deque.empty());
    std::vector<int> elements = {1, 2, 3};
    for (auto x : elements) {
        deque.push_back(x);
    }
    REQUIRE(!deque.empty());
    REQUIRE(deque.front() == elements[0]);
    deque.pop_front();
    REQUIRE(deque.front() == elements[1]);
    deque.pop_front();
    deque.pop_front();
    REQUIRE(deque.empty());
    REQUIRE(deque.front() == noValue);
}

TEST_CASE("Pitch works") {
    int sampleRate = 48000;
    oddvoices::Pitch pitch(sampleRate);
    pitch.setDriftLFOAmplitude(0);
    pitch.setJitterAmplitude(0);
    pitch.setVibratoMaxAmplitude(0);
    float portamentoTime = 0.1;
    pitch.setBasePortamentoTime(portamentoTime);
    REQUIRE(pitch.process() == 0);
    float frequency = 440;
    pitch.setTargetFrequency(frequency);
    REQUIRE(pitch.process() == frequency);
}

TEST_CASE("Pitch::setTargetFrequency works, ascending") {
    int sampleRate = 48000;
    oddvoices::Pitch pitch(sampleRate);
    pitch.setDriftLFOAmplitude(0);
    pitch.setJitterAmplitude(0);
    pitch.setVibratoMaxAmplitude(0);
    pitch.setOvershootTimeRatio(0);
    pitch.setOvershootAmount(0);
    pitch.setPreparationTimeRatio(0);
    pitch.setPreparationAmount(0);
    float portamentoTime = 0.1;
    pitch.setBasePortamentoTime(portamentoTime);
    float frequency1 = 440;
    float frequency2 = 880;
    pitch.setTargetFrequency(frequency1);
    pitch.process();
    pitch.setTargetFrequency(frequency2);
    // * 2 because the portamento time is doubled for an octave jump.
    for (int i = 0; i < sampleRate * portamentoTime * 2; i++) {
        float frequency = pitch.process();
        REQUIRE(frequency1 <= frequency);
        REQUIRE(frequency <= frequency2);
    }
    pitch.process();
    REQUIRE(pitch.process() == frequency2);
}

TEST_CASE("Pitch::setTargetFrequency works, descending") {
    int sampleRate = 48000;
    oddvoices::Pitch pitch(sampleRate);
    pitch.setDriftLFOAmplitude(0);
    pitch.setJitterAmplitude(0);
    pitch.setVibratoMaxAmplitude(0);
    pitch.setOvershootTimeRatio(0);
    pitch.setOvershootAmount(0);
    pitch.setPreparationTimeRatio(0);
    pitch.setPreparationAmount(0);
    float portamentoTime = 0.1;
    pitch.setBasePortamentoTime(portamentoTime);
    float frequency1 = 880;
    float frequency2 = 440;
    pitch.setTargetFrequency(frequency1);
    pitch.process();
    pitch.setTargetFrequency(frequency2);
    // * 2 because the portamento time is doubled for an octave jump.
    for (int i = 0; i < sampleRate * portamentoTime * 2; i++) {
        float frequency = pitch.process();
        REQUIRE(frequency2 <= frequency);
        REQUIRE(frequency <= frequency1);
    }
    pitch.process();
    REQUIRE(pitch.process() == frequency2);
}

// See https://github.com/catchorg/Catch2/blob/devel/docs/own-main.md#adding-your-own-command-line-options.
int main(int argc, char* argv[])
{
    Catch::Session session;
    using namespace Catch::Clara;
    auto cli = session.cli()
        | Opt(g_cmudict, "cmudict")["--cmudict"]("Path to CMU Pronouncing Dictionary (required)");
    session.cli(cli);
    int returnCode = session.applyCommandLine(argc, argv);
    if (returnCode != 0) {
        return returnCode;
    }
    if (!std::filesystem::exists(g_cmudict)) {
        std::cerr << "Please set --cmudict option to point to a file." << std::endl;
        return 1;
    }
    return session.run();
}
