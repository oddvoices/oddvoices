#include <vector>
#include <cstdint>

namespace oddvoices {
namespace frontend {

void write16BitMonoWAVFile(
    std::string fileName
    , int sampleRate
    , const std::vector<int16_t>& data
);

} // namespace frontend
} // namespace oddvoices
