#include <algorithm>
#include <cmath>
#include <optional>
#include <vector>

#include "rapidjson/document.h"
#include "MidiFile.h"

#include "sing.hpp"
#include "liboddvoices.hpp"
#include "g2p.hpp"
#include "wav.hpp"

namespace oddvoices {
namespace frontend {

using Iterator = rapidjson::Value::ConstMemberIterator;

enum class EventType
{
    SetFrequencyImmediate,
    SetTargetFrequency,
    NoteOn,
    SetFormantShift,
    SetPhonemeSpeed,
    Empty,
    NoteOnRT,
    NoteOffRT,
};

struct Event
{
    EventType type;
    float seconds;
    // Used in SetTargetFrequency and SetFrequencyImmediate only.
    float frequency = 0;
    // Used in NoteOn only.
    float endSeconds = 0;
    // Used in SetFormantShift only.
    float formantShift = 1;
    // Used in SetPhonemeSpeed only.
    float phonemeSpeed = 1;

    Event(EventType type, float seconds)
        : type(type)
        , seconds(seconds)
    {
    }
};

std::pair<bool, std::string> sing(
    oddvoices::Voice& voice
    , oddvoices::g2p::G2P& g2p
    , const std::vector<Event>& events
    , float duration
    , std::string outWAV
    , std::string text
)
{
    auto phonemes = g2p.pronounce(text);

    float sampleRate = voice.getSampleRate();

    std::vector<int> segmentQueue = (
        voice.convertPhonemesToSegmentIndices(phonemes)
    );

    oddvoices::Synth synth(
        sampleRate
        , &voice
        , segmentQueue.data()  // Segment queue memory
        , segmentQueue.size()  // Segment queue capacity
        , 0  // Segment queue initial starting point
        , segmentQueue.size()  // Segment queue initial size
    );

    int numSamples = sampleRate * duration;
    std::vector<int16_t> samples(numSamples);

    int timeInSamples = 0;
    double timeInSeconds = 0;
    for (auto const& event : events) {
        int numSamplesToProcess = (event.seconds - timeInSeconds) * sampleRate;
        for (int j = 0; j < numSamplesToProcess; j++) {
            samples[timeInSamples] = synth.process();
            timeInSamples += 1;
        }
        timeInSeconds += static_cast<float>(numSamplesToProcess) / sampleRate;
        if (event.type == EventType::NoteOn) {
            synth.noteOn(event.endSeconds - event.seconds);
        } else if (event.type == EventType::NoteOnRT) {
            synth.noteOn();
        } else if (event.type == EventType::NoteOffRT) {
            synth.noteOff();
        } else if (event.type == EventType::SetTargetFrequency) {
            synth.setTargetFrequency(event.frequency);
        } else if (event.type == EventType::SetFrequencyImmediate) {
            synth.setFrequencyImmediate(event.frequency);
        } else if (event.type == EventType::SetFormantShift) {
            synth.setFormantShift(event.formantShift);
        } else if (event.type == EventType::SetPhonemeSpeed) {
            synth.setPhonemeSpeed(event.phonemeSpeed);
        }
    }
    while (timeInSamples < numSamples) {
        samples[timeInSamples] = synth.process();
        timeInSamples += 1;
    }

    write16BitMonoWAVFile(outWAV, sampleRate, samples);

    return std::make_pair(true, "");
}

std::pair<bool, std::string> singJSON(
    oddvoices::Voice& voice
    , oddvoices::g2p::G2P& g2p
    , std::string inJSON
    , std::string outWAV
    , std::string inText
)
{
    rapidjson::Document document;
    if (document.Parse(inJSON.c_str()).HasParseError()) {
        return std::make_pair(false, "Invalid JSON.");
    }
    std::string lyrics = inText;
    Iterator lyricsIterator = document.FindMember("lyrics");
    if (lyricsIterator != document.MemberEnd()) {
        const rapidjson::Value& jsonLyrics = lyricsIterator->value;
        if (!jsonLyrics.IsString()) {
            return std::make_pair(false, "Lyrics must be a string.");
        }
        if (lyrics == "") {
            lyrics = jsonLyrics.GetString();
        }
    }
    Iterator eventsIterator = document.FindMember("events");
    if (eventsIterator == document.MemberEnd()) {
        return std::make_pair(false, "Missing required field 'events'.");
    }
    const rapidjson::Value& jsonEvents = eventsIterator->value;
    if (!jsonEvents.IsArray()) {
        return std::make_pair(false, "'events' must be an array.");
    }
    std::vector<Event> events;
    std::optional<int> noteOnEventIndex;
    float lastTime = 0;
    for (rapidjson::SizeType i = 0; i < jsonEvents.Size(); i++) {
        const rapidjson::Value& jsonEvent = jsonEvents[i];
        if (!jsonEvent.IsObject()) {
            return std::make_pair(false, "Events must be objects.");
        }

        Iterator typeIterator = jsonEvent.FindMember("type");
        if (typeIterator == jsonEvent.MemberEnd()) {
            return std::make_pair(false, "Event is missing 'type' field.");
        }
        if (!typeIterator->value.IsString()) {
            return std::make_pair(false, "Event type must be a string.");
        }
        std::string type = typeIterator->value.GetString();

        Iterator timeIterator = jsonEvent.FindMember("time");
        if (timeIterator == jsonEvent.MemberEnd()) {
            return std::make_pair(false, "Event is missing 'time' field.");
        }
        if (!timeIterator->value.IsNumber()) {
            return std::make_pair(false, "Event time must be a number.");
        }
        float time = timeIterator->value.GetFloat();

        if (time < 0 || std::isinf(time) || std::isnan(time)) {
            return std::make_pair(false, "Event time must be a nonnegative number.");
        }
        if (time < lastTime) {
            return std::make_pair(false, "Events must be in chronological order.");
        }
        lastTime = time;

        if (type == "noteOn") {
            if (!noteOnEventIndex.has_value()) {
                noteOnEventIndex = events.size();
                events.emplace_back(EventType::NoteOn, time);
            }
        } else if (type == "noteOff") {
            if (noteOnEventIndex.has_value()) {
                events[noteOnEventIndex.value()].endSeconds = time;
                noteOnEventIndex = {};
            }
        } else if (type == "noteOnRT") {
            events.emplace_back(EventType::NoteOnRT, time);
        } else if (type == "noteOffRT") {
            events.emplace_back(EventType::NoteOffRT, time);
        } else if (type == "setTargetFrequency") {
            Iterator frequencyIterator = jsonEvent.FindMember("frequency");
            if (frequencyIterator == jsonEvent.MemberEnd()) {
                return std::make_pair(false, "Event is missing 'frequency' field.");
            }
            if (!frequencyIterator->value.IsNumber()) {
                return std::make_pair(false, "Frequency must be a number.");
            }
            float frequency = frequencyIterator->value.GetFloat();
            events.emplace_back(EventType::SetTargetFrequency, time);
            events.back().frequency = frequency;
        } else if (type == "setFrequencyImmediate") {
            Iterator frequencyIterator = jsonEvent.FindMember("frequency");
            if (frequencyIterator == jsonEvent.MemberEnd()) {
                return std::make_pair(false, "Event is missing 'frequency' field.");
            }
            if (!frequencyIterator->value.IsNumber()) {
                return std::make_pair(false, "Frequency must be a number.");
            }
            float frequency = frequencyIterator->value.GetFloat();
            events.emplace_back(EventType::SetFrequencyImmediate, time);
            events.back().frequency = frequency;
        } else if (type == "setFormantShift") {
            Iterator formantShiftIterator = jsonEvent.FindMember("formantShift");
            if (formantShiftIterator == jsonEvent.MemberEnd()) {
                return std::make_pair(false, "Event is missing 'formantShift' field.");
            }
            if (!formantShiftIterator->value.IsNumber()) {
                return std::make_pair(false, "Formant shift must be a number.");
            }
            float formantShift = formantShiftIterator->value.GetFloat();
            events.emplace_back(EventType::SetFormantShift, time);
            events.back().formantShift = formantShift;
        } else if (type == "setPhonemeSpeed") {
            Iterator phonemeSpeedIterator = jsonEvent.FindMember("phonemeSpeed");
            if (phonemeSpeedIterator == jsonEvent.MemberEnd()) {
                return std::make_pair(false, "Event is missing 'phonemeSpeed' field.");
            }
            if (!phonemeSpeedIterator->value.IsNumber()) {
                return std::make_pair(false, "Phoneme speed must be a number.");
            }
            float phonemeSpeed = phonemeSpeedIterator->value.GetFloat();
            events.emplace_back(EventType::SetPhonemeSpeed, time);
            events.back().phonemeSpeed = phonemeSpeed;
        } else if (type == "empty") {
            events.emplace_back(EventType::Empty, time);
        } else {
            return std::make_pair(false, "Unrecognized event type.");
        }
    }

    return sing(voice, g2p, events, lastTime, outWAV, lyrics);
};


char k_midiLyrics = 0x05;
char k_midiEndOfTrack = 0x2f;

float convertMIDINoteToHertz(float midiNote)
{
    return 440 * std::pow(2.0f, (midiNote - 69) / 12);
}

static bool hasNoteEvent(const smf::MidiEventList& eventList)
{
    for (int i = 0; i < eventList.size(); i++) {
        auto& event = eventList[i];
        if (event.isNoteOn() || event.isNoteOff()) {
            return true;
        }
    }
    return false;
}

std::pair<bool, std::string> singMIDI(
    oddvoices::Voice& voice
    , oddvoices::g2p::G2P& g2p
    , std::string inMIDI
    , std::string outWAV
    , std::string inText
)
{
    smf::MidiFile midiFile;
    midiFile.read(inMIDI);
    if (!midiFile.status()) {
        return std::make_pair(false, "Failed to read MIDI file.");
    }

    midiFile.removeEmpties();
    midiFile.doTimeAnalysis();
    midiFile.linkNotePairs();

    int trackCount = midiFile.getTrackCount();
    if (trackCount == 0) {
        return std::make_pair(false, "No tracks in MIDI file.");
    }
    int trackIndex;
    for (trackIndex = 0; trackIndex < midiFile.size(); trackIndex++) {
        auto& track = midiFile[trackIndex];
        if (hasNoteEvent(track)) {
            break;
        }
    }
    if (trackIndex == midiFile.size()) {
        return std::make_pair(false, "Couldn't find a track that has note events.");
    }
    auto& track = midiFile[trackIndex];

    bool useMIDILyrics = inText == "";
    std::string text = inText;

    std::optional<float> timeOfTrackEndInSeconds;
    for (int i = 0; i < track.size(); i++) {
        auto& event = track[i];
        if (useMIDILyrics) {
            // event.isMeta() guarantees that event.size() >= 3, so there is no out of bounds
            // issue here.
            if (event.isMeta() && event[1] == k_midiLyrics) {
                // length (event[2]) is ignored and inferred from the size of the vector.
                for (int j = 3; j < static_cast<int>(event.size()); j++) {
                    text.push_back(event[j]);
                }
                text.push_back(' ');
            }
        }
        if (event.isMeta() && event[1] == k_midiEndOfTrack) {
            timeOfTrackEndInSeconds = event.seconds;
        }
    }
    if (!timeOfTrackEndInSeconds.has_value()) {
        return std::make_pair(false, "Invalid MIDI file.");
    }

    std::vector<int> notesOn;
    std::vector<Event> events;
    std::optional<int> currentMelismaIndex;
    for (int i = 0; i < track.size(); i++) {
        auto& event = track[i];
        if (event.isNoteOn()) {
            int midiNote = event[1];
            float frequency = convertMIDINoteToHertz(midiNote);
            if (notesOn.empty()) {
                events.emplace_back(EventType::NoteOn, event.seconds);
                currentMelismaIndex = events.size() - 1;
            }
            notesOn.push_back(midiNote);
            events.emplace_back(EventType::SetTargetFrequency, event.seconds);
            events.back().frequency = frequency;
        } else if (event.isNoteOff()) {
            int midiNote = event[1];
            notesOn.erase(
                std::remove_if(notesOn.begin(), notesOn.end(), [midiNote](int x) {
                    return x == midiNote;
                }),
                notesOn.end()
            );
            if (notesOn.empty() && currentMelismaIndex.has_value()) {
                events[currentMelismaIndex.value()].endSeconds = event.seconds;
            } else {
                float frequency = convertMIDINoteToHertz(notesOn.back());
                events.emplace_back(EventType::SetTargetFrequency, event.seconds);
                events.back().frequency = frequency;
            }
        }
    }

    return sing(
        voice,
        g2p,
        events,
        timeOfTrackEndInSeconds.value(),
        outWAV,
        text
    );
}

} // namespace frontend
} // namespace oddvoices
