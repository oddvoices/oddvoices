#include <string>
#include "../src/g2p.hpp"
#include "../src/liboddvoices.hpp"

namespace oddvoices {
namespace frontend {

/// Given a Voice, a G2P, a path to a JSON file, and some input text,
/// run the singing synthesizer and produce an output WAV file.
///
/// Returns a pair consisting of a Boolean value that indicates whether the
/// operation was successful, and a string containing an error message.
///
/// If the inText is an empty string, use lyrics in the JSON. Ideally we
/// would use std::optional for this, but this function needs to be called
/// by WebAssembly, and embind does not support std::optional.
std::pair<bool, std::string> singJSON(
    oddvoices::Voice& voice
    , oddvoices::g2p::G2P& g2p
    , std::string inJSON
    , std::string outWAV
    , std::string inText
);

/// Given a Voice, a G2P, a path to a MIDI file, and some input text,
/// run the singing synthesizer and produce an output WAV file.
///
/// Returns a pair consisting of a Boolean value that indicates whether the
/// operation was successful, and a string containing an error message.
///
/// If the inText is an empty string, use MIDI lyric events. Ideally we
/// would use std::optional for this, but this function needs to be called
/// by WebAssembly, and embind does not support std::optional.
std::pair<bool, std::string> singMIDI(
    oddvoices::Voice& voice
    , oddvoices::g2p::G2P& g2p
    , std::string inMIDIFile
    , std::string outWAV
    , std::string inText
);

} // namespace frontend
} // namespace oddvoices
