#include <fstream>

#include "wav.hpp"

namespace oddvoices {
namespace frontend {

static void write16BitIntegerLE(std::ostream& stream, uint16_t value)
{
    stream
        << static_cast<unsigned char>(value & 0xff)
        << static_cast<unsigned char>((value >> 8) & 0xff);
}

static void write32BitIntegerLE(std::ostream& stream, uint32_t value)
{
    stream
        << static_cast<unsigned char>(value & 0xff)
        << static_cast<unsigned char>((value >> 8) & 0xff)
        << static_cast<unsigned char>((value >> 16) & 0xff)
        << static_cast<unsigned char>((value >> 24) & 0xff);
}

void write16BitMonoWAVFile(
    std::string fileName
    , int sampleRate
    , const std::vector<int16_t>& data
)
{
    std::ofstream stream(fileName, std::ios::binary);

    uint32_t dataSizeInBytes = data.size() * sizeof(int16_t);
    uint32_t fileSizeInBytes = 4 + 4 + 16 + 4 + 4 + dataSizeInBytes;

    int lengthOfFormatBlockInBytes = 16;
    int bitsPerSample = 16;
    int numChannels = 1;
    int formatTagPCM = 1;
    int averageBytesPerSecond = sampleRate * bitsPerSample * numChannels / 8;
    int dataBlockSize = (bitsPerSample * numChannels) / 8;

    stream << "RIFF";
    write32BitIntegerLE(stream, fileSizeInBytes);
    stream << "WAVE";
    stream << "fmt ";
    write32BitIntegerLE(stream, lengthOfFormatBlockInBytes);
    write16BitIntegerLE(stream, formatTagPCM);
    write16BitIntegerLE(stream, numChannels);
    write32BitIntegerLE(stream, sampleRate);
    write32BitIntegerLE(stream, averageBytesPerSecond);
    write16BitIntegerLE(stream, dataBlockSize);
    write16BitIntegerLE(stream, bitsPerSample);
    stream << "data";
    write32BitIntegerLE(stream, dataSizeInBytes);

    for (auto x : data) {
        write16BitIntegerLE(stream, x);
    }
}

} // namespace frontend
} // namespace oddvoices
