name: Actions

on:
  push:
    branches: [develop]
  pull_request:
    branches: [develop]
  release:
    types: [published]

env:
  BUILD_TYPE: Release
  PYTHON_VERSION: 3.9
  CMUDICT: cmudict-0.7b

jobs:
  linux:
    runs-on: ubuntu-latest
    steps:
    - uses: actions/checkout@v2
      with:
        submodules: recursive
        lfs: true
    - name: Set up Python
      uses: actions/setup-python@v2
      with:
        python-version: ${{env.PYTHON_VERSION}}
    - name: Install dependencies
      run: |
        sudo apt-get install cmake g++ libsndfile1 valgrind
        python3 -m pip install --upgrade pip
        python3 -m pip install flake8
        python3 -m pip install -e .[dev]
    - name: Lint with flake8
      run: |
        # stop the build if there are Python syntax errors or undefined names
        flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics
        # exit-zero treats all errors as warnings. The GitHub editor is 127 chars wide
        flake8 . --count --exit-zero --max-complexity=10 --max-line-length=127 --statistics
    - name: Lint with black
      run: black --check .
    - name: Check types with mypy
      run: mypy python/oddvoices tests
    - name: Configure CMake
      run: cmake -S cpp -B cpp/build -DCMAKE_BUILD_TYPE=${{env.BUILD_TYPE}}
    - name: Build project
      run: cmake --build cpp/build
    - name: Run C++ tests
      run: ./cpp/build/oddvoices_test --cmudict ${{env.CMUDICT}}
    - name: Test with pytest
      run: pytest --sing-executable=cpp/build/sing
    - name: Compile voices
      run: python3 scripts/compile_voices.py
    - name: Prepare artifact upload
      run: |
        mkdir oddvoices
        cp cpp/build/sing oddvoices
        cp cmudict-0.7b oddvoices
        cp LICENSE oddvoices
        cp README_artifacts.txt oddvoices/README
        cp -r compiled_voices oddvoices
        tar -czvf oddvoices_linux_x64.tar.gz oddvoices
    - name: Upload artifacts
      uses: actions/upload-artifact@v2
      with:
        name: oddvoices_linux_x64.tar.gz
        path: ${{github.workspace}}/oddvoices_linux_x64.tar.gz
        retention-days: 7

  windows:
    runs-on: windows-2019
    steps:
    - uses: actions/checkout@v2
      with:
        submodules: recursive
        lfs: true
    - name: Set up Python
      uses: actions/setup-python@v2
      with:
        python-version: ${{env.PYTHON_VERSION}}
    - name: Install Python repo
      run: |
        py -3 -m pip install --upgrade pip
        py -3 -m pip install -e .[dev]
    - name: Configure CMake
      run: cmake -S cpp -B cpp/build -G "Visual Studio 16 2019" -A x64
    - name: Build project
      run: cmake --build cpp/build --config ${{env.BUILD_TYPE}}
    - name: Run C++ tests
      run: ./cpp/build/${{env.BUILD_TYPE}}/oddvoices_test.exe --cmudict ${{env.CMUDICT}}
    - name: Run pytest
      run: py -3 -m pytest --sing-executable=cpp/build/${{env.BUILD_TYPE}}/sing.exe
    - name: Compile voices
      run: py -3 scripts/compile_voices.py
    - name: Copy files to artifact region
      run: |
        mkdir oddvoices
        cp cpp/build/${{env.BUILD_TYPE}}/sing.exe oddvoices
        cp cmudict-0.7b oddvoices
        cp LICENSE oddvoices
        cp README_artifacts.txt oddvoices/README
        cp -r compiled_voices oddvoices
    - name: Upload artifacts
      uses: actions/upload-artifact@v2
      with:
        name: oddvoices_windows_x64.zip
        path: ${{github.workspace}}/oddvoices
        retention-days: 7

  macos:
    runs-on: macos-11
    steps:
    - uses: actions/checkout@v2
      with:
        submodules: recursive
        lfs: true
    - name: Set up Python
      uses: actions/setup-python@v2
      with:
        python-version: ${{env.PYTHON_VERSION}}
    - name: Install Python repo
      run: |
        python3 -m pip install --upgrade pip
        python3 -m pip install -e .[dev]
    - name: Configure CMake
      run: cmake -S cpp -B cpp/build -DCMAKE_BUILD_TYPE=${{env.BUILD_TYPE}}
    - name: Build project
      run: cmake --build cpp/build
    - name: Run C++ tests
      run: ./cpp/build/oddvoices_test --cmudict ${{env.CMUDICT}}
    - name: Test with pytest
      run: pytest --sing-executable=./cpp/build/sing
    - name: Compile voices
      run: python3 scripts/compile_voices.py
    - name: Prepare artifact upload
      run: |
        mkdir oddvoices
        cp cpp/build/sing oddvoices
        cp cmudict-0.7b oddvoices
        cp LICENSE oddvoices
        cp README_artifacts.txt oddvoices/README
        cp -r compiled_voices oddvoices
        tar -czvf oddvoices_macos_x64.tar.gz oddvoices
    - name: Upload artifacts
      uses: actions/upload-artifact@v2
      with:
        name: oddvoices_macos_x64.tar.gz
        path: ${{github.workspace}}/oddvoices_macos_x64.tar.gz
        retention-days: 7

  deploy_gh:
    if: startsWith(github.ref, 'refs/tags/')
    needs: [linux, macos, windows]
    runs-on: ubuntu-latest
    env:
      DOWNLOAD_PATH: ${{github.workspace}}/download
    steps:
      - name: Download artifacts
        uses: actions/download-artifact@v2
        with:
          path: ${{env.DOWNLOAD_PATH}}
      - name: Rezip downloaded Windows artifact
        run: |
          cd ${{env.DOWNLOAD_PATH}}
          mv oddvoices_windows_x64.zip oddvoices_windows_x64
          zip -r oddvoices_windows_x64.zip oddvoices_windows_x64
      - name: Upload to the release page
        uses: softprops/action-gh-release@v1
        with:
          files: |
            ${{env.DOWNLOAD_PATH}}/oddvoices_linux_x64.tar.gz/*.tar.gz
            ${{env.DOWNLOAD_PATH}}/oddvoices_macos_x64.tar.gz/*.tar.gz
            ${{env.DOWNLOAD_PATH}}/oddvoices_windows_x64.zip
          draft: true
